# Basic config
SERVICE_NAME="Bitbucket Python Worker"
SERVICE_NAME_NO_SPACES=bitbucket-python-worker
VERSION_SUFFIX ?= development
DOCKER_PATH ?= bitbucket
VERSION=$(shell git rev-parse --short HEAD)-$(VERSION_SUFFIX)
DOCKER_REMOTE=docker.atlassian.io/$(DOCKER_PATH)/$(SERVICE_NAME_NO_SPACES)

# Micros related stuff
MICROS_ENV ?= dev-west2

all: build

dev: dockercompose-build

build: docker-build sd-build

%.sd:  %.sd.tmpl .git/HEAD
	m4 -DTAG=$(VERSION) -DDOCKER_PATH=$(DOCKER_PATH) $< > $@

sd-build: bitbucket-pytnon-worker.sd

docker-build:
	docker build . -t $(DOCKER_REMOTE):$(VERSION)

dockercompose-build:
	docker-compose build

docker-push:
	docker push $(DOCKER_REMOTE):$(VERSION)

deploy: docker-push sd-build
	micros service:deploy bitbucket-python-worker -e $(MICROS_ENV)

clean:
	rm -rf build

test-deps:
	pip install -U --no-deps -r requirements-test.txt

lint: test-deps
	flake8 .

test: test-deps
	mkdir -p /test_results
	DJANGO_SETTINGS_MODULE=bbpw.settings_test pytest --junitxml=/test_results/test.xml --cov --cov-config .coveragerc .

docker-test:
	docker-compose build test && docker-compose run test

devbg:
	docker-compose up -d db sqs redis && sleep 3

devfore:
	docker-compose up web tasker worker

devbuildup: dockercompose-build devbg devfore

devdownhard:
	docker-compose stop && docker-compose rm -f

devreloadworker:
	docker-compose stop worker && docker-compose up worker

devrebuild: devdownhard devbuildup

devenv:
	docker-compose exec web /bin/bash

.PHONY: build deploy clean dockercompose-build docker-build docker-push sd-build dev all deps test-deps lint test
